

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ambientes {
    private String bloco;
    private String nome_ambiente;
    private String latitude;
    private String longitude;
    private String acessivel; 

    public String getBloco() {
        return bloco;
    }

    public void setBloco(String bloco) {
        this.bloco = bloco;
    }

    public String getNome_ambiente() {
        return nome_ambiente;
    }

    public void setNome_ambiente(String nome_ambiente) {
        this.nome_ambiente = nome_ambiente;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAcessivel() {
        return acessivel;
    }

    public void setAcessivel(String acessivel) {
        this.acessivel = acessivel;
    }

    public String armazenar(){
        
        try {
            //Armazena o arquivo txt dentro da pasta do projeto
            FileWriter fw = new FileWriter("Ambientes.txt", true);

            //Escreve cada um dos campos no arquivo
            PrintWriter pw = new PrintWriter(fw);
            pw.println("Bloco: "+this.bloco);
            pw.println("Descrição: "+this.nome_ambiente);
            pw.println("Latitude: "+this.latitude);
            pw.println("Longitude: "+this.longitude);
            pw.println("Acessivel: "+this.acessivel);
            pw.println("----------------------------");
            
            //Envia os dados do formulário imediatamente 
            pw.flush();
            
            //Fechando PW e FW
            pw.close();
            fw.close();

            
        } catch (IOException ex) {
            Logger.getLogger(Ambientes.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "Enviado com sucesso!";
    }
}
